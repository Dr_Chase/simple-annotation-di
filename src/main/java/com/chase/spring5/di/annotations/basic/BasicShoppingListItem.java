package com.chase.spring5.di.annotations.basic;

import com.chase.spring5.di.annotations.abstracts.AbstractShoppingListItem;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor @ToString @EqualsAndHashCode
public class BasicShoppingListItem extends AbstractShoppingListItem {

}
