package com.chase.spring5.di.annotations.abstracts;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.chase.spring5.di.annotations.interfaces.Shop;
import com.chase.spring5.di.annotations.interfaces.ShoppingList;
import com.chase.spring5.di.annotations.interfaces.ShoppingListItem;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString (callSuper=true, includeFieldNames=true)
@EqualsAndHashCode
public abstract class AbstractShoppingList implements ShoppingList {
	
	protected List<ShoppingListItem> shoppingList = new ArrayList<>();

	@Override
	public List<ShoppingListItem> getShoppingList() {
		
		return shoppingList;
	}

	@Override
	public Double getTotalPrice() {
		Double output = 0.0;
		
		for(ShoppingListItem sli : shoppingList) {
			
			output += sli.getPriceForQuantity();
		}
		return output;
	}

	@Override
	public List<Shop> getShopsBoughtFrom() {
		
		//List<Shop> output = new ArrayList<>(shoppingList.size());
		
		Set<Shop> shopSet = new HashSet<>();
		
		for(ShoppingListItem sli : shoppingList) {
			
			shopSet.add(sli.getShopPurchasedFrom());
		}
		
		return new ArrayList<>(shopSet);
	}

}
