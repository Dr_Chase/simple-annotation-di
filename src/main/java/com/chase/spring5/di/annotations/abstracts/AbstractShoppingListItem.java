package com.chase.spring5.di.annotations.abstracts;

import java.util.Map.Entry;

import com.chase.spring5.di.annotations.interfaces.Shop;
import com.chase.spring5.di.annotations.interfaces.ShoppingListItem;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString (callSuper=true, includeFieldNames=true)
@EqualsAndHashCode
public abstract class AbstractShoppingListItem implements ShoppingListItem{
	
	protected String itemName;
	protected Double quantity;
	protected Entry<Double, Double> entryOfQuantityKeyAndPriceValue;
	protected Shop shopPurchasedFrom;

	public String getItemName()	{
		
		return itemName;
	}

	public Double getQuantity()	{
		
		return quantity;
	}

	public Entry<Double, Double> getPricePerQuantity(){
		
		return entryOfQuantityKeyAndPriceValue;
	}
	
	public Shop getShopPurchasedFrom() {
		
		return shopPurchasedFrom;
	}

	// TODO check this method when you are sober
	public Double getPriceForQuantity() {
		
		Double baseQuantity = getPricePerQuantity().getKey();
		
		Double priecPerQuantity = getPricePerQuantity().getValue();
		
		Double returnValue = (priecPerQuantity / baseQuantity) * getQuantity();
		
		return returnValue;
	}
	
	public Shop getShopBoughtFrom() {
		
		return shopPurchasedFrom;
	}
}
