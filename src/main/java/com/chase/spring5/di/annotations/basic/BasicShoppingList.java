package com.chase.spring5.di.annotations.basic;

import org.springframework.stereotype.Component;

import com.chase.spring5.di.annotations.abstracts.AbstractShoppingList;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Component
@NoArgsConstructor @ToString @EqualsAndHashCode
public class BasicShoppingList extends AbstractShoppingList {

	private String nameOfList;

	public String getNameOfList() {
		return nameOfList;
	}

	public void setNameOfList(String nameOfList) {
		this.nameOfList = nameOfList;
	}
	
	
}
