package com.chase.spring5.di.annotations.abstracts;

import com.chase.spring5.di.annotations.interfaces.Shop;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString (callSuper=true, includeFieldNames=true)
@EqualsAndHashCode
public abstract class AbstractShop implements Shop {
	
	protected String shopAddress;
	protected String branding;
	protected Boolean retail;
	protected Boolean discount;
	protected Double discountInPercentage;

	@Override
	public String getShopAddress() {
		return shopAddress;
	}

	@Override
	public String getBranding() {
		return branding;
	}

	@Override
	public Boolean isRetail() {
		return retail;
	}

	@Override
	public Boolean isDiscount() {
		return discount;
	}

	@Override
	public Double getDiscountInPercentage() {
		return discountInPercentage;
	}

	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}

	public void setBranding(String branding) {
		this.branding = branding;
	}

	public void setRetail(Boolean retail) {
		this.retail = retail;
	}

	public void setDiscount(Boolean discount) {
		this.discount = discount;
	}

	public void setDiscountInPercentage(Double discountInPercentage) {
		this.discountInPercentage = discountInPercentage;
	}

}
