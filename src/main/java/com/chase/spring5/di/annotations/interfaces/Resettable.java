package com.chase.spring5.di.annotations.interfaces;

public interface Resettable {
	void reset();
}
