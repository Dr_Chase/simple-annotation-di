package com.chase.spring5.di.annotations.basic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.chase.spring5.di.annotations.abstracts.AbstractShoppingListService;
import com.chase.spring5.di.annotations.interfaces.ShoppingList;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Component
@NoArgsConstructor
@ToString (callSuper=true, includeFieldNames=true)
@EqualsAndHashCode
public class BasicShoppingListService extends AbstractShoppingListService {

	@Autowired
	public BasicShoppingListService(ShoppingList shoppingList) {
		super();
		this.shoppingList = shoppingList;
	}
}
