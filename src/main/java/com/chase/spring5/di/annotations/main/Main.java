package com.chase.spring5.di.annotations.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.chase.spring5.di.annotations.interfaces.ShoppingListService;

public class Main {

	// run on commandline with:
	// mvn exec:java -Dexec.mainClass="com.chase.spring5.di.annotations.main.Main"
	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("config-01.xml");
		ShoppingListService userService = context.getBean(ShoppingListService.class);
		
		System.out.println(userService);
		
		System.out.println(userService.getShoppingList());
	}

}
