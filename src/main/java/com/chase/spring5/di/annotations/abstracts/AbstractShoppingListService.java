package com.chase.spring5.di.annotations.abstracts;

import java.util.List;
import java.util.stream.Collectors;

import com.chase.spring5.di.annotations.interfaces.ShoppingList;
import com.chase.spring5.di.annotations.interfaces.ShoppingListService;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString (callSuper=true, includeFieldNames=true)
@EqualsAndHashCode
public abstract class AbstractShoppingListService implements ShoppingListService {

	protected ShoppingList shoppingList;

	@Override
	public List<String> getShopNames() {
		
		return   shoppingList.getShoppingList()
				.stream()
				.map( item -> item.getShopPurchasedFrom().getBranding())
				.collect(Collectors.toList());
	}

	@Override
	public List<String> getBoughtItemNames() {
		
		return  shoppingList.getShoppingList()
				.stream()
				.map( item -> item.getItemName())
				.collect(Collectors.toList());
	}

	// basic no discounts
	@Override
	public Double getTotalPaidPrice() {
		
		return shoppingList.getTotalPrice();
	}

	@Override
	public ShoppingList getShoppingList() {
		
		return shoppingList;
	}

}
