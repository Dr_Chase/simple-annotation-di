package com.chase.spring5.di.annotations.basic;

import com.chase.spring5.di.annotations.abstracts.AbstractShop;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor @ToString @EqualsAndHashCode
public class BasicShop extends AbstractShop {

}
