package com.chase.spring5.di.annotations.interfaces;

import java.util.List;

public interface ShoppingListService {

	public List<String> getShopNames();
	public List<String> getBoughtItemNames();
	public Double getTotalPaidPrice();
	
	public ShoppingList getShoppingList();
}
