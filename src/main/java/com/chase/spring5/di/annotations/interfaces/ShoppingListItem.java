package com.chase.spring5.di.annotations.interfaces;

import java.util.Map.Entry;

public interface ShoppingListItem {

	public String getItemName();

	public Double getQuantity();

	/**
	 * 
	 * @return Entry with first quantity as key, and second price as value
	 */
	public Entry<Double, Double> getPricePerQuantity();
	
	public Shop getShopPurchasedFrom();

	public Double getPriceForQuantity();
}
