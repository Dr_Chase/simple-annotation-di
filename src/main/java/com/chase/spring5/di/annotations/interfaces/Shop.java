package com.chase.spring5.di.annotations.interfaces;

public interface Shop {
	public String getShopAddress();
	public String getBranding();
	public Boolean isRetail();
	public Boolean isDiscount();
	public Double getDiscountInPercentage();
}
