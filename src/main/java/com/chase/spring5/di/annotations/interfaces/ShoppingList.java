package com.chase.spring5.di.annotations.interfaces;

import java.util.List;

public interface ShoppingList {

	public List<ShoppingListItem> getShoppingList();
	
	public Double getTotalPrice();
	
	public List<Shop> getShopsBoughtFrom();
	
	public String getNameOfList();
	
}
