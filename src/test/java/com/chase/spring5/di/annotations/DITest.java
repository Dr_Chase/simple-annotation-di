package com.chase.spring5.di.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import com.chase.spring5.di.annotations.interfaces.ShoppingListService;

@ContextConfiguration(locations = "/config-01.xml")
public class DITest extends AbstractTestNGSpringContextTests{

	@Autowired
    ApplicationContext context;
	
	@Autowired
	ShoppingListService sli;
	
	@Test
	public void testConfiguration()	{
		
		assertNotNull(context);
		
        Set<String> definitions = new HashSet<>(
                Arrays.asList(context.getBeanDefinitionNames())
        );
        
        System.out.println(definitions);
        assertTrue(definitions.contains("basicShoppingListService"));
        
	}
}
