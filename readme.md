Simple example that shows some basic spring dependency injection.

build: "mvn clean install" or "mvn clean package"
run: mvn exec:java -Dexec.mainClass="com.chase.spring5.di.annotations.main.Main"

TODO set up injection context in test

In this example the beans.xml (here config-01.xml) is necessary. Alternatively we could 
set up configuration classes, and add them to the main class.

The example consist of 5 interfaces and 4 abstract classes. It's a model for an imaginary 
shopping list app, which could store a shopping list in for example in a webapp, or a smartphone.

Take note the goal is to make dependency injection work, and write different examples around it,
so the model is not that sophisticated, because it serves as a DI example.

